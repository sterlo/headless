# Contributing

## Overview

We're going to start with the following:

1. Read Only: doing things that write, involves authentication. I can be lazy by just eliminating that from this experiment :)
2. Vanilla JavaScript™: We're not going to use frameworks here. Anyone who is familiar with a framework will hopefully understand normal JavaScript. Anyone who isn't familiar with a framework, can still benefit.
3. We will test our results the best we can. There appear to be very little options in terms of emulating search engines and how they are currently crawling anything. It's apparently not in ANY of their best interests to allow developers to have any sort of CLI options -- so I'm going to use Web Master tools to QA this work. I swear I am not bitter.

----

As of building this, WordPress core merely supports infrastructure for a REST API.
Source: https://wordpress.org/news/2015/12/clifford/

It references a plugin, the official WordPress REST API plugin: https://wordpress.org/plugins/rest-api/

Source: http://wp-api.org/

I'm going to not install that. I'm going to look under the hood, understand how it works, write my own thingy.
There's just a few magic files to understand, they all start from `wp-settings.php`, which in the grand scheme of WordPress -- is the Geoffrey Chaucer of this Knight's Tale.

```
require( ABSPATH . WPINC . '/rest-api.php' );
require( ABSPATH . WPINC . '/rest-api/class-wp-rest-server.php' );
require( ABSPATH . WPINC . '/rest-api/class-wp-rest-response.php' );
require( ABSPATH . WPINC . '/rest-api/class-wp-rest-request.php' );
```

Learn these.

## Do's and Don'ts

Title looks weird huh? Bugs the crap out of me.

ANYWAY!

### Do's

* Understand the core code base, read it from top to bottom, or you're going to have a bad time.
* Create your own namespace for your extensions.
* Use this codebase as a reference for learning

### Don'ts

* Don't run your business or hedge your long term bets on this codebase. Shit will change and shit will break.


## Notes

There's documentation: http://v2.wp-api.org/extending/adding/
Out of the box, wp-json now works.
You'll want a namespace.

Here is a "hello world".

```
add_action( 'rest_api_init', function () {
    register_rest_route( 'headless', '/test', array(
        'methods' => 'GET',
        'callback' => 'hello_world',
    ) );
} );

function hello_world( $data ) {
    return array('hello world');
}
```

Place that in your Theme or w/e and then visit `/wp-json/headless/test`, this will output "hello world" and let you know that things are off to a good start!

Now I do have a hard time registering a home route...that is a route at "/".

This makes sense, because that route is pre-defined when you create your namespace. It's also, not super useful in terms of architecture to have a root endpoint that does something, in my opinion anyway.

Alright... we have a namespace ... and an endpoint ... now what?
None of that was hard. So, what is? (heh)

We need to generate the header/footer for our WordPress templates. This will produce the minimal amount of code we need in order to be a valid DOM structure. That's important right?

They are included in this repo, has header.php and footer.php -- because that's the god damn WordPress way. They are bundled in an index.php burrito.

By the way, did I mention that all the shit we're going to produce here will pass those fantastic, ever so valuable, code validators? BECAUSE THEY TOTALLY WILL TILL THEY DON'T!

OK -- so now that we have a DOM structure, we need to identify the important things that will be output by the server. Namely, any meta information that a search engine or crawler might care about. Dat sweet meta.

@TODO -- define a ton of meta stuff.

We also want to remove the cruft.

@TODO -- remove BS header/footer content.

After that's done, we need to make our robots.txt rules and we need to make our shebang paths.
You may notice something at this point. If we're going to be doing shebangs (heh) are we not doubling effort on the wp-json route definitions? Probably, but the wp-json routes are for getting data from WordPress. That's a fraction of a Single Page Application.

We're going to want special routes for talking to things that are not "stately". We'll also have wp-json endpoints we communicate to using that logic -- this is not a bad thing.

@TODO -- create a model system, or leverage a framework.
