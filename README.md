# Headless

GREAT SCOTT, what am I doing?!

## Mindset

The aim of this experiment is not to build a fully featured WordPress theme that looks amazing. It's to create a working example of this fabled model in WordPress, this decoupled thing ... that really people keep talking about -- but there's no real examples live. That sucks, it's going to cause a lot of pain in the industry, so the mindset here is, solving pain, not solving all of the worlds problems.

I predict we can make a Single Page Application (SPA) that leverages the core features of WordPress and is also indexable by search engines.

The known ways (or at least what I know about) are the following:

* Pre-rendering of Client Side code, a form of taking site snapshots. Things like Node/Phantom combos. This can be an external services or an internal solution.
* Routed alternative content. This, can be done via any sort of identifier, such as subdomains, port numbers, query parameters, and so on. This can be done using User Agent detection, robots.txt files, sitemaps, and on and on and on.
* Leveraging established rules of a narrow set, albeit arguably the most important set, of search engines. Examples: https://developers.google.com/webmasters/ajax-crawling/docs/learn-more, https://developers.google.com/webmasters/ajax-crawling/docs/getting-started and https://developers.google.com/webmasters/ajax-crawling/docs/specification

Now that last one, is really documentation around one service. Other search engines may not work. That may not matter, because in the current world of the web, Google is the current weapon of choice when it comes to search engines.

## Pre-Rendering

Since this isn't particularly hard to do. I'm not going to experiment with it. With this, you create your Single Page Application, you use something like Node/Phantom or a service and BAM, as long as you provide this content to just the search engines, there's no magic here.

## Routed Content & Google's Rules

This will be my playground. Not one, but both. Why? Because I can.

I'm thinking, that robots.txt is widely supported, and we should drive all search engines to alternative content via that. We'll need URL structures to support just search engines for getting content, but I'd also like to support `#!` as it's a perfectly valid ask and won't hurt the little search engines that could and can expose more content to Google.

Some of the risks here are, duplicating content. Don't do that -- that's bad, hurts your score.
Indexing search engine URLs instead of proper ones. Don't do that -- that's bad -- fixable, but I hate the thought of targeting user agents.

## Contributing

The first ten million commits were the worst. And the second ten million: they were the worst, too. The third ten million I didn't enjoy at all. After that, I went into a bit of a decline.

If you'd like to join, head on over to the [contribution](CONTRIBUTING.md) party.
