<?php

add_action( 'rest_api_init', function () {
    register_rest_route( 'headless', '/test', array(
        'methods' => 'GET',
        'callback' => 'hello_world',
    ) );
} );

function hello_world( $data ) {
    return array('hello world');
}
